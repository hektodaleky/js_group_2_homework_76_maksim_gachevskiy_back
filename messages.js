const express = require('express');
const router = express.Router();
const nanoid = require('nanoid');


const createRouter = (db) => {

    router.get('/', (req, res) => {

        res.send('All')
    });


    router.get('/messages', (req, res) => {
        res.send(db.getLastMessage(req.query))
    });

    router.post('/messages', (req, res) => {
        const messageBody = req.body;
        if (!messageBody.author || messageBody.author.length === 0) {
            res.status(400)
                .send({error: "Author must be present in the request"});
            return;
        }
        if (!messageBody.message || messageBody.message.length === 0) {
            res.status(400)
                .send({error: "Message must be present in the request"});
            return;
        }

        const message = {
            id: nanoid(),
            date: new Date().toISOString(),
            author: messageBody.author,
            message: messageBody.message
        };

        db.addMessage(message).then(() => res.send(message))

    });

    return router;
};

module.exports = createRouter;