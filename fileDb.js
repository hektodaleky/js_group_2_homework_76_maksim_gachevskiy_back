const fs = require('fs');

let data;

module.exports = {
    init: () => {
        return new Promise((resolve, reject) => {
            fs.readFile('./db.json', (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    data = JSON.parse(result);
                    resolve();
                }
            });
        });
    },

    getLastMessage: (queryItem) => {


        const last = data.sort(function (a, b) {
            return new Date(a.date) - new Date(b.date);
        });
        if (queryItem.date) {
            return last.filter(item => new Date(item.date) > new Date(queryItem.date));
        }
        if (last.length > 30)
            last.length = 30;
        return last;

    },

    addMessage: (item) => {
        data.push(item);
        let contents = JSON.stringify(data, null, 2);

        return new Promise((resolve, reject) => {
            fs.writeFile('./db.json', contents, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve(item);
                }
            });
        });
    }


};